This project is a template that can be used as starting point in Unity projects. Just clone it before starting working on new project, and then develop 
your game based on this structure.

# Project content

You are given:

### 1. Project folders general structures

   - Animation 
      - Clips
	  - Controllers
   - Audio 
      - Music
	  - Sounds
   - Prefabs
   - Management
   - UI
   - World
   - Scenes
   - Scripts
      - Core
	  - Game
   - Resources
   - Data
   - Fonts
   - Materials
   - Sprites
 
 
### 2. 4 scenes

   - Logo
   - Main menu 
   - Game
   - Final
	
	
### 3. Predefind scene hierarchy

   - UI
   - World
   - Management
   
### 4. Main tags & layers
 

 